var gulp = require('gulp');;
var cssimport = require('gulp-cssimport');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var del = require('del');

var css_input = './scss/main.scss';
var css_output = 'main.scss.liquid';
var js_input = 'js/main.js';
var js_output = 'main.js.liquid';
var folder_output = './assets/';

// cleanup build file
gulp.task('clean', function() {
    return del([
        folder_output + css_output,
        folder_output + js_output
    ]);
});

// Combining SCSS files
gulp.task('styles', function() {
    gulp.src([css_input])
        .pipe(cssimport())
        .pipe(rename(css_output))
        .pipe(gulp.dest(folder_output));
});

// Combining JavaScript files
gulp.task('scripts', function() {
    gulp.src(js_input)
		.pipe(browserify({
		    insertGlobals : true,
		    debug : !gulp.env.production
		}))
        .pipe(rename(js_output))
		.pipe(gulp.dest(folder_output))
});

// gulp watch
gulp.task('watch', function() {
    gulp.watch('scss/**/*.scss', ['styles']);
    gulp.watch('js/**/*.js', ['scripts']);
});

// default task
gulp.task('default', ['clean', 'styles', 'scripts']);

