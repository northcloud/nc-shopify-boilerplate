Skeleton theme
============

The Skeleton theme is a simplified Shopify theme, to be used as a "blank slate" starting point for theme designers.

###Features:
- Almost no theme settings. Ready to be customized any way you want. 
- Only ~500 lines of CSS including comments. 
- Despite its 500 lines of CSS code, it is responsive and has styled drop-down menus.
- Include SVG images to style select elements and cart icon.
- Commented code to teach you Liquid concepts in practice.

###Demo:

- [Demo store](http://skeleton.myshopify.com/)

Getting started
---------------------
1. [Download](https://bitbucket.org/northcloud/nc-shopify-boilerplate/get/master.zip) the latest version
2. or clone the git repo: ```git clone git@bitbucket.org:northcloud/nc-shopify-boilerplate.git```

Basic structure
---------------
```
├── assets
│   └── Javascript, CSS, and theme images
├── config
│   └── custom Theme Settings
├── layout
│   ├── theme.liquid
│   └── optional alternate layouts
├── snippets
│   └── optional custom code snippets
├── templates
│   ├── 404.liquid
│   ├── article.liquid
│   ├── blog.liquid
│   ├── cart.liquid
│   ├── collection.liquid
│   ├── index.liquid
│   ├── page.liquid
│   ├── product.liquid
│   └── search.liquid
│   └── list-collections.liquid
```

Additional resources
---------------------
- [Free workshops](http://meetup.shopify.com/): Sign up for a free Shopify For Designers workshop in a city near you.
- [Theme Documentation](http://docs.shopify.com/themes): Learn more about Liquid and theme templates.
- [Desktop Theme Editor](http://apps.shopify.com/desktop-theme-editor): For Mac OS X users, we recommend our free app to sync theme files in development. 